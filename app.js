const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const config = require('./config');
const { interserviceKey, interserviceAuthExpiration, appId, api } = require('./config');

const getStarted = require('./setting/get-started');
const menu = require('./setting/menu');
const greeting = require('./setting/greeting');

const ErrorHandler = require('./middlewares/error-handler');
const EnableCors = require('./middlewares/enable-cors');
const AdminAuth = require('./middlewares/admin-auth');
const ExpressBootstrapper = require('./modules/express-bootstrapper');
const fbVerification = require('./middlewares/fb-verification');
const apiAcces = require('./middlewares/acces');
const Controler = require('./modules/controler-class');

const Fb = require('./modules/fb');
const ApiClient = require('./modules/client/api-client');
const FmklServiceClient = require('./modules/client/fmkl-service-client');
const PayloadHandler = require('./modules/handlers/payload-handler');
const Watcher = require('./modules/handlers/watcher');
const TextMessage = require('./modules/handlers/text-message');
const PostBack = require('./modules/handlers/post-back');
const QuickReply = require('./modules/handlers/quick-reply');
const Flow = require('./modules/flow');
const ActionManager = require('./modules/action-manager');
const MQManager = require('./modules/m-queue-manager');

//Routes
const Routes = require('./routes');
const FacebookRoute = require('./routes/facebook');
const AdminApi = require('./routes/admin-api');

//models
const actionModel = require('./models/action-model');
const messageQueueModel = require('./models/message-queue-model');

const errorHandler = new ErrorHandler();
const enableCors = new EnableCors();
const mQManager = new MQManager({ messageQueueModel });
const fb = new Fb(config.token, config, { mQManager });
const watcher = new Watcher({ mQManager, fb });
const actionManager = new ActionManager({ actionModel });
const payloadHandler = new PayloadHandler(config.payloadMap, config.payloadSeparator);
const apiClient = new ApiClient(config.api);
const fmklServiceClient = new FmklServiceClient(
  {
    interserviceKey,
    interserviceAuthExpiration,
    appId,
    fmklServiceHost: api.fmklServiceHost
  },
  { fb }
);
const textMessage = new TextMessage();
const postBack = new PostBack({ payloadHandler });
const quickReply = new QuickReply({ payloadHandler });
const controler = new Controler(
  {
    fb,
    watcher,
    textMessage,
    postBack,
    quickReply
  }
);
const flow = new Flow(
  { actionModel },
  {
    postBack,
    quickReply,
    textMessage,
    apiClient,
    fb,
    actionManager,
    mQManager,
    fmklServiceClient
  }
);

flow.track();

const adminAuth = new AdminAuth({ fmklServiceClient });


const mongoose = require('mongoose');
mongoose.Promise = Promise;
const connectionString = config.MONGO_STRING;
// Mongo-connection
mongoose.connect(connectionString, { useNewUrlParser: true });

// Bootstrap
const expressBootstrapper = new ExpressBootstrapper(
  { errorHandler, enableCors }
);
expressBootstrapper.bootstrap();

const facebookRoute = new FacebookRoute(
  { fbVerification },
  { controler, fb, mQManager, fmklServiceClient }
);

const adminApi = new AdminApi(
  { adminAuth },
  { apiClient, actionManager, fmklServiceClient }
);

const routes = new Routes(
  [
    facebookRoute,
    adminApi
  ],
  errorHandler
);

routes.initRoutes(expressBootstrapper.app);

expressBootstrapper.app.listen(config.port, () => {
  console.log(`Bot api started on port ${config.port}`);
});
