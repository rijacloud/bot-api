module.exports = Object.freeze({
  TYPE: {
    INFO_FORME: 1,
    LOGGER: 2
  },
  FIELDS: {
    INFO_FORME: ['paymentOption', 'email', 'phone', 'name']
  },
  SUBTYPE: {
    LOGGER: {
      DISCOVER_CAR: 1,
      COMMUNITY: 2,
      FAQ: 3
    }
  }
});