module.exports = {

  /**
   * regex pattern used to validate comma-separated email-addresses
   * according to http://emailregex.com/
   */
  commaSeparatedEmailPattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))(,\s?(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))*$/,

  /**
   * regex pattern used to validate email-addresses
   * according to http://emailregex.com/
   */
  emailPattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,

  /**
   * Reference: https://stackoverflow.com/questions/161738/what-is-the-best-regular-expression-to-check-if-a-string-is-a-valid-url
   */
  url: /((http|https):\/\/)(([\d\w]|%[a-fA-F\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?/,

  /**
   * According to accepted and explanation in comments: https://stackoverflow.com/questions/20988446/regex-for-mongodb-objectid
   */
  objectIdPattern: /^[a-f\d]{24}$/i,

  /**
   * Phone number regexp
   */
  phoneNumberPattern:  /^[+]?\d[\d -]+\d$/,

  /**
   * Phone code regexp
   */
  phoneCodePattern: /^\+\d{1,3}$/,

  /**
   * malagasy phone code pattern from scrath
   */
  malagasyPhoneCode: /^(\+261|0)[0-9]{7}/,

  /**
   * regex pattern used to validate ISO8601-formatted date-string
   */
  datePattern: /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/,

  /**
   * regex pattern used to validate moment-duration
   * Should support 2 formats:
   * - 00:00 - 23:59
   * - 1d - 99d
   */
  durationPattern: /^(((?!0)(\d\d?)d)|(?!00:00)([0-1]\d|2[0-3]):[0-5]\d)$/,

  /**
   * Password pattern to ckeck that the pwd have 1 upper, 1 lower and 1 number and at least 8 chars
   */
  passwordPattern:  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/,

  /**
   * YYYY-MM-DD date pattern pattern
   */
  reversedDatePattern:   /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/
};
