const httpStatus = require('http-status');

module.exports = class AdminAuth {
  constructor(modules) {
    this._fmklServiceClient = modules.fmklServiceClient;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const authorization = req.headers.authorization;
      if (!authorization) return this._missingAuthorization(res);
      await this._fmklServiceClient.verifyAdminToken(authorization);
      return next();
    } catch (e) {
      if (e === 'TokenExpiredError') return this._tokenExpired(res);
      if (e === 'JsonWebTokenError') return this._invalidToken(res);
      return next(e);
    }
  }

  _missingAuthorization(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Missing authorization'
    });
  }

  _invalidToken(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Invalide token'
    });
  }

  _tokenExpired(res) {
    return res.status(httpStatus.UNAUTHORIZED).send({
      error: true,
      code: httpStatus.UNAUTHORIZED,
      message: 'Expired token'
    });
  }
}