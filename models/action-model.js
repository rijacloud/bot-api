const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  type: { type: Number, required: true },
  fbid: { type: String, required: true },
  state: { type: Object },
  expected: { type: Number, required: true },
  resolved: { type: Boolean, default: false }
}, {
  timestamps: true
});
const Action = mongoose.model('action', schema);

module.exports = Action;