const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  fbId: { type: String, required: true },
  queue: { type: [Object], required: true }
}, {
  timestamps: true
});
const MessageQueue = mongoose.model('messageQueue', schema);

module.exports = MessageQueue;
