const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;

const schema = new Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  locale: { type: String, required: true },
  timezone: { type: Number, required: true },
  gender: { type: String, required: true },
  id: { type: String, required: true }
}, {
  timestamps: true
});
const User = mongoose.model('user', schema);

module.exports = User;