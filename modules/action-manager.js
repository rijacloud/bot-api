const ACTIONS = require('../constants/actions');
const _ = require('lodash');
const moment = require('moment');

module.exports = class ActionManager {
  constructor(models) {
    this._actionModel = models.actionModel;
    this._processMap = {
      [ACTIONS.TYPE.INFO_FORME]: '_processInfoForm'
    };
  }

  hasOngoingAction(fbid) {
    return this._actionModel.countDocuments({ fbid, resolved: false });
  }

  getPendingAction(fbid) {
    return this._actionModel.findOne(
      { fbid, resolved: false }
    ).sort({ _id: -1 }).lean();
  }

  async getPrompedInfo(fbid) {
    const action = await this._actionModel.findOne({
      type: ACTIONS.TYPE.INFO_FORME,
      fbid,
      resolved: true
    }).sort({ _id: -1 }).lean();
    if (!action) return;
    return action.state;
  }

  getResolvedByType(type) {
    return this._actionModel.find({
      type, resolved: true
    }).lean();
  }

  async process(fbid, payload) {
    const action = await this._actionModel.findOne(
      { fbid, resolved: false }
    ).sort({ _id: -1 }).lean();
    return this[this._processMap[action.type]](action, payload);
  }

  create({ type, fbid, expected, state = {} }) {
    return this._actionModel.create({ type, fbid, state, expected });
  }

  logAction(fbid, state) {
    return this._actionModel.create({
      type: ACTIONS.TYPE.LOGGER,
      fbid,
      state,
      resolved: true,
      expected: 0
    });
  }

  getActiveUser() {
    const now = moment().toDate();
    const oneHoureBefor = moment().subtract(1, 'hours').toDate();
    return this._actionModel.find({
      createdAt: { $gte: oneHoureBefor }
    }).sort({ _id: -1 }).lean();
  }

  getLastUsers() {
    const now = moment().toDate();
    const oneDayBefor = moment().subtract(1, 'days').toDate();
    return this._actionModel.find({
      createdAt: { $gte: oneDayBefor }
    }).sort({ _id: -1 }).limit(20).lean();
  }

  async countActiveUser() {
    const activeUsers = await this.getActiveUser();
    if (!activeUsers.length) return 0;
    return _.uniq(activeUsers.map(action => action.fbid)).length;
  }

  _processInfoForm(action, payload) {
    const { input } = payload;
    const newState = _.merge({}, action.state);
    newState[ACTIONS.FIELDS.INFO_FORME[action.expected]] = input;
    const resolved = action.expected === ACTIONS.FIELDS.INFO_FORME.length - 1;
    const expected = action.expected + (resolved ? 0 : 1);
    return this._actionModel.findOneAndUpdate(
      { _id: action._id },
      {
        $set: {
          state: newState,
          expected,
          resolved,
        }
      },
      { new: true, useFindAndModify: true }
    );
  }
}