const request = require('request-promise');

module.exports = class ApiClient {
  constructor(config) {
    this._url = config.url;
  }

  addUser(user) {
    return this._post('api/users/add', user);
  }

  countUsers() {
    return this._get('api/users/count');
  }

  getUserByfbId(fbId) {
    return this._get(`api/users/${fbId}`)
  }

  getCars() {
    return this._get('api/voitures/list');
  }

  getCarById(id) {
    return this._get(`api/voitures/${id}`);
  }

  getCommunityList() {
    return this._get('bot/community/list');
  }

  _get(url) {
    return request({
      url: `${this._url}/${url}`,
      timeout: 120000,
      method: 'GET'
    })
  }

  getMotorsList() {
    return this._get('bot/moteur/list');
  }

  verifyToken(token) {
    return request({
      headers: {
        Authorization: token
      },
      url: `${this._url}/verify-token`,
      timeout: 120000,
      method: 'GET'
    })
  }

  _post(url, data) {
    return request({
      url: `${this._url}/${url}`,
      timeout: 120000,
      method: 'POST',
      json: data
    })
  }
}