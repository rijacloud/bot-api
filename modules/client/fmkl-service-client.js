const request = require('request-promise');
const jwt = require('jsonwebtoken');

module.exports = class FmklServiceClient {
  constructor(config, modules) {
    this._url = config.fmklServiceHost;
    this._interserviceKey = config.interserviceKey;
    this._interserviceAuthExpiration = config.interserviceAuthExpiration;
    this._appId = config.appId;
    this._fb = modules.fb;
  }

  auth() {
    return jwt.sign({ appId: this._appId }, this._interserviceKey, {
      expiresIn: this._interserviceAuthExpiration
    });
  }

  getUser() {
    return this._get('users/list');
  }

  createUser(data) {
    return this._post('users/add', data);
  }

  getBlockByName(name, type) {
    return this._get(`blocks?type=${type}&name=${name}`);
  }

  countUsers() {
    return this._get(`users/count`);
  }

  getUserByfbId(fbid) {
    return this._get(`users/details/${fbid}`);
  }

  verifyAdminToken(auth) {
    return this._get(`admin/${this._appId}/verify-token`, auth);
  }

  getContainer(containerId, sender) {
    this._fb.typing(sender);
    return this._get(`blocks/container/${containerId}`);
  }

  _post(url, data) {
    return request({
      url: `${this._url}/${url}`,
      headers: {
        authorization : this.auth()
      },
      timeout: 120000,
      method: 'POST',
      json: data
    })
  }

  _get(url, auth) {
    return request({
      url: `${this._url}/${url}`,
      headers: {
        authorization : auth || this.auth()
      },
      timeout: 120000,
      method: 'GET',
      json: true
    })
  }
}