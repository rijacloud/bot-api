module.exports = class Controler {
  constructor(modules) {
    this._fb = modules.fb;
    this._textMessage = modules.textMessage;
    this._postBack = modules.postBack;
    this._quickReply = modules.quickReply;
    this._watcher = modules.watcher;

    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const input = req.body.entry[0].messaging;
      for(let i = 0; i < input.length; i++){
        const event = input[i];
        const sender = event.sender.id;
        const user = await this._fb.getUser(
          sender,
          ['first_name', 'last_name', 'locale', 'timezone', 'gender', 'profile_pic']
        );
        if (this._isTextMessage(event)) {     
          await this._textMessage.handler(
            user,
            event.message.text
          ); 
        } else if (this._isPostBack(event)) {
          await this._postBack.handler(
            user,
            event.postback
          );
        } else if (this._isQuickReply(event)) {
          await this._quickReply.handler(
            user, 
            event.message.quick_reply
          );
        } else if (this._isDelivery(event)) {
          await this._watcher.capture(event);
        }    
      }
    } catch (e) {
      console.log(e);
    } finally {
      res.sendStatus(200)
    }
  }

  _handleError(e) {

  }

  _isTextMessage(event) {
    return event.message && event.message.text && !event.message.quick_reply;
  }

  _isPostBack(event) {
    return !!event.postback;
  }

  _isQuickReply(event) {
    return event.message && event.message.quick_reply;
  }

  _isDelivery(event) {
    return !!event.delivery;
  }
}