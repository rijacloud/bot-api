const compiler = require('./templateCompiler');
const template = require('../template');
const priceHandler = require('./price-handler');
const formatNumber = require('simple-format-number');
const { TYPE } = require('../constants/blocks');
/*params: {
  'it\' a test',
  [
    { title: 'button 1', payload: 'test_get_list_foo_bar' },
    { title: 'button 1', payload: 'test_get_list_foo1_bar1' }
  ]
}*/

const quickReplyButtons = (text, params) => {
    const result = {
      text: text,
      quick_replies: []
    };
    params.forEach(param => {
      const hold = {
          content_type: 'text',
          title: param.title,
          payload: param.payload
        };
      if (param.image_url) hold.image_url = param.image_url;
      result.quick_replies.push(hold);
    });
  return result;
}

/*{
    "attachment":{
      "type":"template",
      "payload":{
        "template_type":"generic",
        "elements":[
           {
            "title":"Welcome!",
            "image_url":"https://petersfancybrownhats.com/company_image.png",
            "subtitle":"We have the right hat for everyone.",
            "default_action": {
              "type": "web_url",
              "url": "https://petersfancybrownhats.com/view?item=103",
              "webview_height_ratio": "tall",
            },
            "buttons":[
              {
                "type":"web_url",
                "url":"https://petersfancybrownhats.com",
                "title":"View Website"
              },{
                "type":"postback",
                "title":"Start Chatting",
                "payload":"DEVELOPER_DEFINED_PAYLOAD"
              }              
            ]      
          }
        ]
      }
    }
  }*/
const genericTemplate = (elements) => {
  return {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'generic',
        elements: elements
      }
    }
  };
};

/*{
    "attachment": {
      "type": "template",
      "payload": {
         "template_type": "media",
         "elements": [
            {
               "media_type": "<image|video>",
               "attachment_id": "<ATTACHMENT_ID>"
            }
         ]
      }
    }    
  }*/
const buildMedia = (elements) => {
  return {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'media',
        elements
      }
    }
  };
};

const dynamikGreating = (template) => {
  return composedTextSlices(template, { userName: 'Foo bar' });
};

const composedTextSlices = (template, params) => {
  return compiler(template, params);
};

const firstTime = (user) => {
  const quickReplies = quickReplyButtons(
    ' 👉',
    [
      { title: 'Configurer votre voiture', payload: 'discover_cars_0_0_0' },
      { title: 'Notre communaute', payload: 'community_0_0_0_0' },
      { title: 'FAQ', payload: 'faq_0_0_0_0' }
    ]
  );
  return [quickReplies];
}

const compile = (blocks) => {
  return blocks.map(block => {
    if (block.type === TYPE.CAROUSSEL) {
      return genericTemplate(block.data);
    }
    if (block.type === TYPE.MEDIA) {
      return buildMedia(block.data);
    }
    return block.data;
  });
};

const discoverCars = (elements, pickAcar) => {
  const textSlice = composedTextSlices(pickAcar);
  const caroussel = genericTemplate(elements);
  return textSlice.concat([caroussel]);
};

const chooseMotor = (elements, param, chooseMotorLabel) => {
  const textSlice = composedTextSlices(chooseMotorLabel);
  const caroussel = genericTemplate(elements);
  return textSlice.concat([caroussel]);
};

const askPrestige = (param) => {
  const textSlice = composedTextSlices(template.askPrestige);
  const quickReplies = quickReplyButtons(
    '👉',
    [
      { title: 'Oui', payload: `customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=${param.ac}&prestige=1&step=done&` },
      { title: 'Non', payload: `customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=${param.ac}&prestige=0&step=done&` }
    ]
  );
  return textSlice.concat([quickReplies]);
}

const askAirConditioner = (param) => {
  return quickReplyButtons(
    'Voulez-vous la climatisation?',
    [
      { title: 'Oui', payload: `customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=1&step=3&` },
      { title: 'Non', payload: `customize_cars_edit_motor=${param.motor}&name=${param.name}&ac=0&step=3&` }
    ]
  );
};

const giveCarPrice = (param, buttons, carPrice, colorsOpt, customizeYourCar) => {
  const slice1 = composedTextSlices(
    carPrice,
    { price: `${formatNumber(priceHandler(param), { symbols: { decimal: ',', grouping: ' ' } })}` }
  );
  const slice2 = composedTextSlices(
    colorsOpt
  );
  const slice3 = composedTextSlices(customizeYourCar);
  return slice1.concat(slice2).concat(slice3).concat([buttons]);
};


/*{
    "attachment":{
      "type":"template",
      "payload":{
        "template_type":"button",
        "text":"What do you want to do next?",
        "buttons":[
          {
            "type":"web_url",
            "url":"https://www.messenger.com",
            "title":"Visit Messenger"
          },
          {
            ...
          },
          {...}
        ]
      }
    }
  }*/
const buttonTemplate = (text, buttons) => {
  return {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'button',
        text,
        buttons
      }
    }
  };
}

const buildFaq = () => {
  const questions = template.faqQuestions;
  return templates = questions.map((qst, index) => {
    return buttonTemplate(
      qst,
      [{ type: 'postback', title: 'Réponse', payload:`faq_response_0_0_${index + 1}`}]
    );
  });
}

const faqResponse = (id) => {
  const text = template.faqResponse[id - 1];
  return composedTextSlices(text);
}

const communityList = (data) => {
  const elements = data.slice(0, 6).map(partner => {
    return {
      title: partner.nom,
      image_url: partner.image_cover,
      default_action: {
        type: "web_url",
        url: partner.lien,
        webview_height_ratio: "tall",
      },
      buttons:[
       {
         type: 'web_url',
         url: partner.lien,
         title: " 🌐 Visiter"
       },             
      ]
    }
  });
  return genericTemplate(elements);
}

askEmail = (askEmail) => {
  return composedTextSlices(askEmail);
};

askPhone = (askPhone) => {
  return composedTextSlices(askPhone);
};

askName = (askName) => {
  return composedTextSlices(askName);
}

endCarFlow = (endCarFlow) => {
  const textSlice = composedTextSlices(endCarFlow);
  return textSlice;
}

inputError = (errorMessage) => {
  return composedTextSlices(errorMessage);
}

const buildPayload = (param, payloadString) => {
   return payloadString.replace(/\$\{param\.(\w*)\}/g, (match, capture) => {
      return param[capture];
  })
};

module.exports = {
  firstTime,
  composedTextSlices,
  quickReplyButtons,
  discoverCars,
  genericTemplate,
  askAirConditioner,
  askPrestige,
  giveCarPrice,
  buildMedia,
  buttonTemplate,
  buildFaq,
  faqResponse,
  communityList,
  chooseMotor,
  askEmail,
  askPhone,
  askName,
  endCarFlow,
  inputError,
  dynamikGreating,
  buildPayload,
  compile
};