const dataBuilder = require('./data-builder');
const ACTIONS = require('../constants/actions');
const REGEXP = require('../constants/regexp');
const BLOCKS = require('../constants/blocks');
const { firstBlockId } = require('../config');

const _ = require('lodash');

module.exports = class Flow {
  constructor(models, modules, config) {
    this._postBack = modules.postBack;
    this._quickReply = modules.quickReply;
    this._textMessage = modules.textMessage;
    this._apiClient = modules.apiClient;
    this._fb = modules.fb;
    this._actionManager = modules.actionManager;
    this._mQmanager = modules.mQManager;
    this._fmklServiceClient = modules.fmklServiceClient;
    this.track = this.track.bind(this);
  }

  track() {

    this._postBack.on('event', async ({ commande, user }) => {
      try {
        if (commande.type === 'start') await this._firstTime(user);

        if (commande.type === 'containers' && commande.action === 'trigger') {
        await this._callContainer(user, commande.subaction);
      }

        if (commande.type === 'discover' && commande.action === 'cars' && commande.subaction === '0') {
          await this._discoverCars(user);
        }
        
        if (
            commande.type === 'discover' &&
            commande.action === 'cars' &&
            commande.subaction === 'choose'
          ) {
          await this._chooseMotor(user, commande.id, commande.param);
        }

        if (commande.type === 'customize' && commande.action === 'cars' && commande.subaction === 'edit') {
          await this._handlerCustomEdition(user, commande.param);
        }

        if (commande.type === 'community') await this._communityList(user);

        if (commande.type === 'faq' && commande.action === '0') await this._sendFaq(user);

        if (commande.type === 'faq' && commande.action === 'response') {
          await this._faqResponse(user, commande.id);
        }

      } catch (e) {
        console.log(e);
      }
    });

    this._quickReply.on('event', async ({ commande, user }) => {

      if (commande.type === 'containers' && commande.action === 'trigger') {
        await this._callContainer(user, commande.subaction);
      }

      if (commande.type === 'discover' && commande.action === 'cars') {
        await this._discoverCars(user);
      }

      if (commande.type === 'customize' && commande.action === 'cars' && commande.subaction === 'edit') {
        await this._handlerCustomEdition(user, commande.param);
      }

      if (commande.type === 'faq') await this._sendFaq(user);

      if (commande.type === 'community') await this._communityList(user);

      if (commande.type === 'payement' && commande.action === 'mode') await this._startInfoForm(user, commande);
    });

    this._textMessage.on('event', async ({ input, user }) => {
      const hasAction = await this._actionManager.hasOngoingAction(user.id);
      if (!hasAction) {
        if (/test/.test(input)) {
          await this._dynamikGreating(user);
        }
      } else {
        await this._handleAction(user.id, { input });
      }
    });
  }

  async _handleAction(fbid, payload) {
    const map = {
      [1]: 'askEmail',
      [2]: 'askPhone',
      [3]: 'askName'
    };
    const pendingAction = await this._actionManager.getPendingAction(fbid);
    const previousMethod = map[pendingAction.expected]
    const proceed = this._checkInput(payload, previousMethod);
    if (!proceed) return this._inputError(fbid, previousMethod);
    const savedAction = await this._actionManager.process(fbid, payload);
    if (savedAction.resolved) {
      return this._endCarFlow(savedAction);
    };
    const method = map[savedAction.expected];
    const { block } = await this._fmklServiceClient.getBlockByName(method, BLOCKS.TYPE.TEXT);
    const toSend = dataBuilder[method](block.data.text);
    return this._fb.sendBatch(fbid, toSend);
  }

  async _callContainer(user, containerId) {
    const { block } = await this._fmklServiceClient.getContainer(containerId, user.id);
    const toSend = dataBuilder.compile(block.data);
    return this._fb.sendBatch(user.id, toSend);
  }

  _checkInput({ input }, method) {
    if (method === 'askName') return true;
    if (method === 'askEmail') return REGEXP.emailPattern.test(input);
    if (method === 'askPhone') return REGEXP.malagasyPhoneCode.test(input.trim());
  }

  async _inputError(fbid, method) {
    const { block } = await this._fmklServiceClient.getBlockByName(`${method}Error`, BLOCKS.TYPE.TEXT);
    const toSend = dataBuilder.inputError(block.data.text);
    return this._fb.sendBatch(fbid, toSend);
  }

  async _endCarFlow(action) {
    const { block } = await this._fmklServiceClient.getBlockByName('endCarFlow', BLOCKS.TYPE.TEXT);
    const toSend = dataBuilder.endCarFlow(block.data.text);
    return this._fb.sendBatch(action.fbid, toSend);
  }

  async _dynamikGreating(user) {
    const { block } = await this._fmklServiceClient.getBlockByName('dynamikGreating', BLOCKS.TYPE.TEXT);
    const toSend = dataBuilder.dynamikGreating(block.data.text);
    return this._fb.sendBatch(user.id, toSend);
  }

  async _startInfoForm(user, commande) {
    const data = {
      type: ACTIONS.TYPE.INFO_FORME,
      fbid: user.id,
      state: {
        [ACTIONS.FIELDS.INFO_FORME[0]]: commande.subaction
      },
      expected: 1
    };
    const result = await this._actionManager.create(data);
    const { block } = await this._fmklServiceClient.getBlockByName('askEmail', BLOCKS.TYPE.TEXT);
    const toSend = dataBuilder.askEmail(block.data.text);
    return this._fb.sendBatch(user.id, toSend);
  }

  async _firstTime(user) {
    await this._fmklServiceClient.createUser(
      {
        ..._.pick(user, ['first_name', 'last_name', 'profile_pic']),
        fbid: user.id
      }
    );
    await this._mQmanager.createQueue(user.id);
    const { block } = await this._fmklServiceClient.getContainer(firstBlockId, user.id);
    const toSend = dataBuilder.compile(block.data);
    return this._fb.sendBatch(user.id, toSend);
  }

  async _discoverCars(user) {
    try {
      const action = await this._logAction(
        user.id,
        {
          subType : ACTIONS.SUBTYPE.LOGGER.DISCOVER_CAR
        }
      );
      const { block } = await this._fmklServiceClient.getBlockByName('discoverCars', BLOCKS.TYPE.CAROUSSEL);
      const textBlock = await this._fmklServiceClient.getBlockByName('pickAcar', BLOCKS.TYPE.TEXT);
      const toSend = dataBuilder.discoverCars(block.data, textBlock.block.data.text);
      return this._fb.sendBatch(user.id, toSend);
    } catch (e) {
      console.log(e);
    }
  }

  async _chooseMotor(user, id, param) {
    try {
      const { block } = await this._fmklServiceClient.getBlockByName('chooseMotor', BLOCKS.TYPE.CAROUSSEL);
      const textBlock = await this._fmklServiceClient.getBlockByName('chooseMotorLabel', BLOCKS.TYPE.TEXT);
      block.data = this._buildCarousselPayload(block.data, param);
      const toSend = dataBuilder.chooseMotor(block.data, param, textBlock.block. data.text);
      return this._fb.sendBatch(user.id, toSend);
    } catch (e) {
      console.log(e);
    }
  }

  _buildCarousselPayload(data, param) {
    return data.map(item => {
      item.buttons = item.buttons.map(button => {
        button.payload = dataBuilder.buildPayload(param, button.payload);
        return button;
      });
      return item;
    });
  }

  _buildQuickReplyPayload(data, param) {
    data.quick_replies = data.quick_replies.map(reply => {
      reply.payload = dataBuilder.buildPayload(param, reply.payload);
      return reply;
    });
    return data;
  }

  async _handlerCustomEdition(user, param) {
    if (param.step === 'done') return this._giveCarPrice(user, param);

    if (param.step === '2') return this._askAirConditioner(user, param);

    if (param.step === '3') return this._askPrestige(user, param);

  }

  async _askAirConditioner(user, param) {
    const { block } = await this._fmklServiceClient.getBlockByName('askAirConditioner', BLOCKS.TYPE.QUICK_REPLY);
    block.data = this._buildQuickReplyPayload(block.data, param);
    const toSend = block.data;
    return this._fb.send(user.id, toSend);
  }

  async _askPrestige(user, param) {
    const { block } = await this._fmklServiceClient.getBlockByName('askPrestige', BLOCKS.TYPE.QUICK_REPLY);
    block.data = this._buildQuickReplyPayload(block.data, param);
    const textBlock = await this._fmklServiceClient.getBlockByName('askPrestige', BLOCKS.TYPE.TEXT);
    const toSend = [textBlock.block.data].concat([block.data]);
    return this._fb.sendBatch(user.id, toSend);
  }

  async _giveCarPrice(user, param) {
    const { block } = await this._fmklServiceClient.getBlockByName('giveCarPrice', BLOCKS.TYPE.QUICK_REPLY);
    const carPriceTxtBlock = await this._fmklServiceClient.getBlockByName('carPrice', BLOCKS.TYPE.TEXT);
    const colorOptTxtBlock = await this._fmklServiceClient.getBlockByName('colorsOpt', BLOCKS.TYPE.TEXT);
    const customizeYourCarTxtBlock = await this._fmklServiceClient.getBlockByName('customizeYourCar', BLOCKS.TYPE.TEXT);

    const toSend = dataBuilder.giveCarPrice(
      param,
      block.data,
      carPriceTxtBlock.block.data.text,
      colorOptTxtBlock.block.data.text,
      customizeYourCarTxtBlock.block.data.text
    );
    return this._fb.sendBatch(user.id, toSend);
  }

  async _sendFaq(user) {
    await this._logAction(
      user.id,
      {
        subType : ACTIONS.SUBTYPE.LOGGER.FAQ
      }
    );
    const toSend = dataBuilder.buildFaq();
    return this._fb.sendBatch(user.id, toSend);
  }

  async _communityList(user) {
    const { body } = JSON.parse(await this._apiClient.getCommunityList());
    await this._logAction(
      user.id,
      {
        subType : ACTIONS.SUBTYPE.LOGGER.COMMUNITY
      }
    );
    const toSend = dataBuilder.communityList(body);
    return this._fb.send(user.id, toSend);
  }

  _faqResponse(user, id) {
    const toSend = dataBuilder.faqResponse(id);
    return this._fb.sendBatch(user.id, toSend);
  }

  _logAction(fbid, state) {
    return this._actionManager.logAction(fbid, state);
  }
}