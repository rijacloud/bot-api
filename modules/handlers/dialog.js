const template = require('../../template');
const compiler = require('../templateCompiler');

module.exports = class Dialog {
  constructor(modules) {
    this._fb = modules.fb;
    this._nlpClient = modules.nlpClient;
  }

  roomCreation(sender, hash) {
    return this._fb.sendText(
      sender,
      compiler(roomCreation, { hash })
    );
  }

  async handle(sender, text) {
    const analyse = await this._detect(text);
    if (!analyse) {
      return this._fb.sendText(
        sender,
        compiler(template.noClue)
      );
    } else if (
      analyse.keyWords &&
      analyse.keyWords.length &&
      analyse.intent === 'ask_price') {
        const key = analyse.keyWords[0].meals[0];
        const templateKey = `ask_${key}_price`;
        return this._fb.sendText(
          sender,
          compiler(template[templateKey])
        );
    }
    return this._fb.sendText(
      sender,
      compiler(template[analyse.intent])
    );
  }

  async _detect(text) {
    const { intent, keyWords } = await this._nlpClient.detecte(text);
    if (!intent.score) return;
    return { ...intent, keyWords };
  }
};