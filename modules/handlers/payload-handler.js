module.exports = class PayloadHandler {
	constructor(map, separator) {
		this.map = map;
		this.separator = separator;
	}

	getCommande(payloadString) {
		const raw = payloadString.split(this.separator);
		const result = raw.reduce((commande, value, index) => {
			commande[this.map[index]] = value;
			return commande;
		}, {});
    if (result.param && result.param !== '0') {
      const splited = result.param.split('&');
      result.param = splited.filter(item => !!item).reduce((holder, current) => {
        let pairs = current.split('=');
        holder[pairs[0]] = pairs[1];
        return holder;
      }, {});
    }
    return result;
	}
}