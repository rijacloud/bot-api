const util = require('util');
const events = require('events');
const EventEmitter = events.EventEmitter;

const PostBack = class {
  constructor(modules) {
    this._payloadHandler = modules.payloadHandler;
    this.handler = this.handler.bind(this);
  }

  handler(user, data) {
    try {
      const commande = this._payloadHandler.getCommande(data.payload);
      this.emit('event', { commande, user });
    } catch (e) {
      throw e;
    }
  }
};

util.inherits(PostBack, EventEmitter);
module.exports = PostBack;