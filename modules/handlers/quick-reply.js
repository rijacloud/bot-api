const util = require('util');
const events = require('events');
const EventEmitter = events.EventEmitter;

const QuickReply = class {
  constructor(modules) {
    this._payloadHandler = modules.payloadHandler;
  }

  async handler(user, data) {
    try {
      const commande = this._payloadHandler.getCommande(data.payload);
      this.emit('event', { commande, user });
    } catch (e) {
      throw e;
    }
  }
};

util.inherits(QuickReply, EventEmitter);
module.exports = QuickReply;