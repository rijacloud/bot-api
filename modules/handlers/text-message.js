const util = require('util');
const events = require('events');
const EventEmitter = events.EventEmitter;

const TextMessage = class {
  constructor() {
    this.handler = this.handler.bind(this);
  }

  async handler(user, input) {
    try {
      this.emit('event', { input, user });
    } catch (e) {
      throw e;
    }
  }
};

util.inherits(TextMessage, EventEmitter);
module.exports = TextMessage;
