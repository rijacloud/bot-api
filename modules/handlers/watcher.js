module.exports = class watcher {
  constructor(modules) {
    this._mQManager = modules.mQManager;
    this._fb = modules.fb;
  }

	async capture(input) {
		const block = await this._mQManager.pull(input.sender.id);
    if (!block) return;
    await this._fb.send(input.sender.id, block);
	}
}
