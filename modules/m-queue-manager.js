const _ = require('lodash');
module.exports = class MQueueManager {
  constructor(models) {
    this._messageQueueModel = models.messageQueueModel;
  }

  async push(fbId, blocks) {
    const filter = { fbId };
    const mQ = await this._findQ(filter.fbId);
    const queue = mQ.queue.concat(blocks);
    return this._messageQueueModel.findOneAndUpdate(
      filter,
      { $set: { queue } },
      { new: true, useFindAndModify: false }
    );
  }

  async pull(fbId) {
    const mQ = await this._findQ(fbId);
    const haveMessages = mQ.queue.length;
    if (!haveMessages) return;
    await this._messageQueueModel.updateOne(
      { fbId },
      { $set: { queue: mQ.queue.slice(1) } },
      { useFindAndModify: false }
    );
    return mQ.queue[0];
  }

  async createQueue(fbId) {
    return (await this._messageQueueModel.findOne({ fbId }).lean()) || 
    (await this._messageQueueModel.create({
      fbId,
      queue: []
    }))._doc;
  }

  async _findQ(fbId) {
    return this._messageQueueModel.findOne(
      { fbId }, { queue: 1 }
    ).lean();
  }
}