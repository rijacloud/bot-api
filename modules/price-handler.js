module.exports = ({ name, motor, ac, prestige}) => {
  ac = parseInt(ac, 10);
  prestige = parseInt(prestige, 10);
  const basePrice = 62400000;
  const config = {
    basePrice: 62400000,
    motor: {
      ['4x2']: 0,
      ['4x4']: 12400000,
      ['4x4bloc']:4900000
    },
    name: {
      ['Pick up']: 0,
      ['Fourgon']: 1900000,
      ['Berline']: 2600000
    },
    features: {
      ac: 2400000,
      prestige: 10800000
    }
  };

  return config.basePrice + config.motor[motor] + config.name[name] +
    (ac ? config.features.ac : 0 )+
    (prestige ? config.features.prestige : 0);
}