const ACTIONS = require('../../constants/actions');
const httpStatus = require('http-status');
const Promise = require('bluebird');
const _ = require('lodash');

module.exports = class AdminStats {
  constructor(modules) {
    this._apiClient = modules.apiClient;
    this._fmklServiceClient = modules.fmklServiceClient;
    this._actionManager = modules.actionManager;
    this._actionSubTypeLabels = {
      1: 'Nos voitures',
      2: 'Notre communaute',
      3: 'FAQ'
    };
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const usersCount = await this._getUsersCount();
      const activeUsersCount = await this._actionManager.countActiveUser();
      const lastActivities = await this._getLastUsersActivities();

      const askCarCount = (await this._actionManager.getResolvedByType(
        ACTIONS.TYPE.INFO_FORME
      )).length ;
      return res.status(httpStatus.OK).json({
        usersCount,
        activeUsersCount,
        lastActivities: lastActivities.filter(activity =>!!activity),
        askCarCount
      });
    } catch (e) {
      return next(e);
    }
  }

  async _getUsersCount() {
    const { count } = await this._fmklServiceClient.countUsers();
    return count;
  }

  async _getLastUsersActivities(){
    const lastActivities = await this._actionManager.getLastUsers();
    return Promise.map(_.uniqBy(lastActivities, 'fbid'), async(action) => {
      let { user } = await this._fmklServiceClient.getUserByfbId(action.fbid);
      return {
        date: action.createdAt,
        name: `${user.last_name} ${user.first_name}`,
        subject: action.type === ACTIONS.TYPE.INFO_FORME ?
          'Demande de voitures' :
          this._actionSubTypeLabels[action.state.subType],
        fbid: action.fbid
      };
    });
  }

}