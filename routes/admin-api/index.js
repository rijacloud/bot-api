const express = require('express');
const AdminStats = require('./admin-stats');
const UserDetail = require('./user-detail');

module.exports = class FacebookRoute {
  constructor(middlewares, modules) {
    this._adminAuth = middlewares.adminAuth;
    this._adminStats = new AdminStats(modules);
    this._userDetail = new UserDetail(modules);
  }

  initRoutes(app) {
    const api = express.Router();
    api.use(this._adminAuth.handler);
    api.get('/stats', this._adminStats.handler);
    api.get('/user-detail/:fbid', this._userDetail.handler);
    app.use('/admin-api',api);
  }
}
