const ACTIONS = require('../../constants/actions');
const _ = require('lodash');
const httpStatus = require('http-status');

module.exports = class UserDetail {
  constructor(modules) {
    this._actionManager = modules.actionManager;
    this._fmklServiceClient = modules.fmklServiceClient;
    this.handler = this.handler.bind(this);
  }

  async handler(req, res, next) {
    try {
      const fbid = req.params.fbid;
      const { user } = await this._getUser(fbid);
      const promptedInfo = await this._actionManager.getPrompedInfo(fbid);
      const response = promptedInfo ?
        _.merge(user, promptedInfo) :
        user;
      return res.status(httpStatus.OK).json({ user: response });
    } catch (e) {
      return next(e);
    }
  }

  async _getUser(fbid) {
    return this._fmklServiceClient.getUserByfbId(fbid);
  }
}