const express = require('express');
const config = require('../../config');
const request = require('request-promise');
const BLOCKS = require('../../constants/blocks');
const dataBuilder = require('../../modules/data-builder');


module.exports = class FacebookRoute {
  constructor(middlewares, modules) {
    this._fbVerification = middlewares.fbVerification;
    this._controller = modules.controler;
    this._fb = modules.fb;
    this._mQmanager = modules.mQmanager
    this._fmklServiceClient = modules.fmklServiceClient
  }

  initRoutes(app) {
    const api = express.Router();
    api.get('/', (req, res) => { res.send('hello world i am a secret bot') });
    api.get('/webhook', this._fbVerification);
    api.post('/webhook', this._controller.handler);
    //TODO add broadcast/trigger 
    api.post('/inter-service/broadcast', async (req, res,next) => {
      
      const datas = req.body.block
      let client = await this._fmklServiceClient.getUser()
      client = client.users
      for(let x = 0 ; x < client.length ; x ++ ) {
        this._fb.typing(client[x].channelId);

        const { block } = await this._fmklServiceClient.getContainer(datas, client[x].channelId);
        const toSend = dataBuilder.compile(block.data);  
        await this._fb.sendBatch(client[x].channelId, toSend);
          
      }
      res.status(200).json({ ok : true });
      
    })
    api.post('/inter-service/menu', (req, res, next) => {
      
      const setting = {
        "persistent_menu":[
          {
            "locale":"default",
            "composer_input_disabled": false,
            "call_to_actions": req.body
          },
          {
            "locale":"zh_CN",
            "composer_input_disabled":false
          }
        ]
      }

      request( {
        url: config.messengerProfile ,
        timeout: 120000,
        qs: { access_token: config.token },
        method: 'POST',
        json: setting
      }).then(response => {
        res.status(200).json({ ok : true });
      }).catch(error => {
        next(error);
      });
    });
    api.post('/inter-service/attachement', async (req, res, next) => {
      try {
        const attachment_id = await this._fb.uploadImage(
          req.body.src,
          req.body.type
        );
        return res.status(200).json({ attachment_id });
      } catch (e) {
        return next(e);
      }
    });
    app.use(api);
  }
  
}

