const request = require('request-promise');
const config = require('../config');

const setting = { 
  get_started: {
    "payload":"start_"
  }
}
const run = async (setting) => {
  const response = await request({
    url: 'https://graph.facebook.com/v2.6/me/messenger_profile',
    timeout: 120000,
    qs: { access_token: config.token },
    method: 'POST',
    json: setting
  })
  console.log(response);
};

try {
  run(setting);
} catch (e) {
  console.log(e);
}
