const request = require('request-promise');
const config = require('../config');

const setting = {
  "greeting":[
    {
      "locale":"default",
      "text":"Bienvenu sur Karenjy!"
    }, {
      "locale":"en_US",
      "text":"Timeless apparel for the masses."
    }
  ] 
};
request({
  url: 'https://graph.facebook.com/v2.6/me/messenger_profile',
  timeout: 120000,
  qs: { access_token: config.token },
  method: 'POST',
  json: setting
}).then(response => {
    console.log(response);
}).catch(error => {
  console.log(error);
});