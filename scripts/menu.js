const config = require('../config');
const request = require('request-promise');

const setting = {
  "persistent_menu":[
    {
      "locale":"default",
      "composer_input_disabled": false,
      "call_to_actions":[
        {
          "title":"🚐  Nos voitures",
          "type":"postback",
          "payload":"discover_cars_0_0_0"
        },
        {
          "title":" 👨‍👩‍👧‍👦  Notre communaute",
          "type":"postback",
          "payload":"community_0_0_0_0"
        },
        {
          "title":"📜 FAQ",
          "type":"postback",
          "payload":"faq_0_0_0_0"
        }
      ]
    },
    {
      "locale":"zh_CN",
      "composer_input_disabled":false
    }
  ]
}

request( {
  url: config.messengerProfile ,
  timeout: 120000,
  qs: { access_token: config.token },
  method: 'POST',
  json: setting
}).then(response => {
  console.log(response);
}).catch(error => {
  console.log(error);
});