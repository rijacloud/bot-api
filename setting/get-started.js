const request = require('request-promise');

module.exports = (req, res) => {

const setting = { 
  get_started: {
    "payload":"start_0_0_0_0"
  }
}

request( {
	url: 'https://graph.facebook.com/v2.6/me/messenger_profile',
	timeout: 120000,
	qs: { access_token: process.env.FB_TOKEN_ZETA_BOT },
	method: 'POST',
	json: setting
}).then(response => {
		console.log(response);
	res.send(response);
}).catch(error => {
	console.log(error);
});

}