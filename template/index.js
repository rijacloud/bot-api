module.exports = {
	//welcomeMessage: `Bonjour {{userName}}, je suis l'assistant Karenjy.`,
  pickAcar: `La MAZANA II, la dernière née des modèle Karenjy a été conçue spécifiquement pour l'environnement de Madagascar 🤗.
-> Elle existe en 3 versions, choisissez celle que vous souhaitez. 🤗`,
  customizeCarLabel: `Vous allez découvrir les différentes options disponibles pour cette version de la MAZANA II.
-> Sélectionnez celles qui correspondent à votre besoin 🤔`,
  ['4x4']: 'un moteur 4X4 explication',
  ['4x2']: 'un moteur 4X2 explication',
  ['4x4bloc']: 'un moteur 4X4 bloc explication',
  askPrestige: `Voulez-vous la version prestige? Vous disposerez des options suivantes : 
👉 Interieur cuir ✔️ 
👉 jante alu ✔️ 
👉 ecran tactile ✔️ 
👉 camera de recul ✔️ 
👉 Vitre teintée ✔️`,
  carPrice: `Dans la configuration choisie, le véhicule coûte {{price}} Ar HT.`,
  colorsOpt: `Pour informations, dans le cas où vous souhaitez personnaliser vos couleurs, l'option est à 2.900.000 Ar HT`,
  faqQuestions: [
    'Qu’en est il de la disponibilité des pièces ?',
    'Quelle est la garantie constructeur ?',
    'Est il possible de louer 1 Karenjy à la journée ?',
    'Est il possible de visiter l’usine ?',
    'Quels sont les caractéristiques techniques de la Mazana II ?',
    'Quels sont les éléments fabriqués à l’usine ?',
    'Le Relais et la démarche d’insertion, qu’est ce que c’est ?'
  ],
  faqResponse: [
    `Toutes les pièces sont en stock et disponible en moins de 24h. 🕰️
-> Toute les pièces sont d’origines constructeur, et grâce à nos différents partenariats et une conception simplifiée, les pièces ont un rapport qualité / prix imbattables !`,
    `La garantie constructeur est de 3 ans ou 100 000km (1er terme atteint).
-> De plus la carrosserie en fibre de verre évite tout risque de corrosion, tout comme un traitement spécifique sur le châssis mécano-soudé`,
    `Bien sur, appelez nos partenaires suivant
-> 📱 032 XXXXXX
-> 📱 033 XXXXXX
-> 📱 034 XXXXXX`,
   `Oui, il est possible de visiter l’usine et les autres activités du Relais (centre de tri et confection textile, usine Karenjy).
-> La visite dure 1 heure, et il y a un droit d’entrée de 10 000Ar/personne.
-> Il est possible d’ajouter la visite du centre de tri et valorisation des déchets à 15mn de voiture (prévoir votre moyen de locomotion).
-> Les visites se font uniquement sur rendez vous ! Merci de prendre contact avec Sitraka au 📱 032 12 510 04 📱 ou par mail 📤 sitraka@lerelais.mg 📤 
Bonne visite !`,
    `La Mazana II a été conçu sur les base d’un véhicule Peugeot Citroen. Le moteur est un 1,6L HDi, turbo, 110ch.
-> Le moteur à été tropicalisé pour tenir compte des contraintes propres à Madagascar, notamment la qualité du carburant.
-> La voiture est légère (1 350kg) ce qui confère un bon rapport poids puissance et de bon dépassement. La direction est assisté, avec un bon rayon de braquage. 
-> La voiture se décline en différente silhouette (pick up, berline, fourgon) 3 ou 6 places ou existe en version 4x2, 4x4 endurance, 4x4 extreme avec la solution DANGEL.
Plus d’information sur notre simulateur en ligne ! 🌐 http://www.karenjy.mg/ 🌐 ou télécharger la plaquette technique
-> http://www.karenjy.mg/sites/5559f1235918ad2b75000b1d/content_entry5559f1b85918ad807f000337/5aa927f95918ade2ba5203e7/files/_Plaquette__MZII.pdf?1575900816`,
    `La Mazana II est le fruit de plusieurs années de R&D et essai par les équipes d’ingénieurs malgaches basées à Fianarantsoa.
-> L’ensemble de la voiture a été conçu localement, permettant d’adapter la voiture aux contraintes locales.
-> Nous fabriquons à Fianarantsoa le chassis (mécano-soudé), la carrosserie (en fibre de verre), les fauteuils, les faisceaux électriques, et l’assemblage complet.
-> Plus de 60 personnes travaillent à la fabrication des voitures.`,
   `Karenjy appartient au groupe Le Relais Madagasikara fondée en 2009, et qui est une entreprise à vocation sociale.
-> Son activité principale est de créer des emplois durables pour les personnes en situation d’exclusion.
-> Aujourd’hui près de 500 emplois ont été créé dans 5 secteurs d’activités (collecte et valorisation des ordures ménagères, tri et confection textile, automobile, hotellerie solidaire, riz).
-> Pour en savoir plus visitez le site 🌐 www.lerelais.mg 🌐`,
  ],
  customizeYourCar: `☝️ Le saviez vous ?
💡 Vous pouvez personnaliser à l’infini la couleur de votre Karenjy ! 🤩
👉 Pour cela, rendez-vous sur le site de Karenjy pour configurer vos choix de couleurs 🌈`,
  askEmail: '📤 Votre email',
  askPhone: '📱 Votre telephone',
  askName: '🧾 Votre nom',
  endCarFlow: 'Nous vous remercions de votre intérêt. Voici quelques contenus qui pourraient vous intéresser',
  chooseMotorLabel: 'Motricité souhaitée',
  askEmailError: 'Merci de saisir une adresse email valide',
  askPhoneError: 'Merci de saisir un numero de telephone valide'
};